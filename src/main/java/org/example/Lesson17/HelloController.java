package org.example.Lesson17;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * SpringMVC-приложение при помощи JAVA-конфигурации
 */

@Controller
public class HelloController {

    /**
     * Когда пользователь обратится по URL: /наше приложение/hello-world/
     * сработает метод sayHello()
     * @return - вернет "Hello_world"
     */
    @GetMapping("/hello-world")
    public String sayHello(){

        //в данном случае возврвщает название файла hello_world.xml
        //а из файла hello_world.xml выводит в браузер Hello___world!
        return "hello_world";
    }
}
