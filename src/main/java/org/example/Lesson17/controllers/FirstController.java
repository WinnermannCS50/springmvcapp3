package org.example.Lesson17.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
/**
 * При GET--запросе на /first/hello сработает метод helloPage()
 * При GET--запросе на /first/goodbye сработает метод goodByePage()
 */
@RequestMapping("/first")
public class FirstController {

    /**
     * При GET--запросе на /hello сработает метод helloPage()
     * @return
     */
    @GetMapping("/hello")
    public String helloPage(){
        return "first/hello";

    }

    /**
     * При GET--запросе на /goodbye сработает метод goodByePage()
     * @return
     */
    @GetMapping("/goodbye")
    public String goodByePage(){
        return "first/goodbye";

    }
}
